@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin panel</a></li>
            @if(Request::is('admin/mailbox/') || Request::is('admin/mailbox') || Request::is('admin/mailbox/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.inbox') }}" >Mailbox</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">Nieuw Bericht</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="h4">Nieuw Bericht</h1>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">

            @include('shared.forms.message')
            </div>
        </div>
    </div>
@endsection
