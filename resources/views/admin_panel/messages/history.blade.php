@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin Panel</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Berichten</a></li>
            <li class="breadcrumb-item active" aria-current="page">Verzonden Berichten</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12 mb-3">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h2 class="h4"><i class="far fa-fw fa-envelope"></i> Verzonden Berichten</h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(isset($messages)&& $messages->first() !== null)
                                <ul class="nav flex-column list-unstyled display-list small" id="tab" role="tablist">
                                    <li>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <strong>Onderwerp</strong>
                                            </div>
                                            <strong>Tijd en Datum</strong>
                                        </div>
                                    </li>
                                    @foreach($messages as $message)
                                        <li>
                                            <a class="nav-link" data-toggle="pill" href="#message-{{$loop->iteration}}" aria-controls="message-{{$loop->iteration}}" aria-selected="true">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <span>{{isset($message->subject) ? $message->subject : ""}}</span>
                                                    </div>
                                                    <span>{{isset($message->created_at) ? $message->created_at : ""}}</span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <p>
                                    Geen berichten gevonden.
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 mb-3">
            @include('shared.panels.history')
        </div>
    </div>
@endsection
