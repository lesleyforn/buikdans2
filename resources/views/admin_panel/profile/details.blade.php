@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin panel</a></li>
            @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                <li class="breadcrumb-item active" aria-current="page">Profiel</li>
            @elseif(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.users') }}" >Gebruikers</a></li>
                <li class="breadcrumb-item active" aria-current="page">Profiel</li>
            @endif
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row no-gutters">
        <div class="col-sm-12">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="h4">Profiel</h1>

                    @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                        <form action="{{route('users.destroy' , $user)}}" method="post" class="btn-group-sm">
                            @csrf
                            @method('DELETE')
                            @if(Auth::user()->hasRole('admin'))
                                <a href="{{route('admin.user.role.edit', $user)}}" class="btn btn-sm btn-outline-dark">Rechten aanpassen</a>
                            @endif
                            <a href="{{route('admin.user.edit', $user)}}" class="btn btn-sm btn-outline-dark">Aanpassen</a>

                            @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                                @if($user->is_active !== 0)
                                    <button type="submit" class="btn btn-sm btn-outline-danger">Deactiveren</button>
                                @else
                                    <a href="{{route('users.activate', $user)}}" class="btn btn-sm btn-outline-dark">Activeren</a>
                                @endif
                            @endif
                        </form>
                    @else
                        <div class="btn-group-sm">
                            <a href="{{route('admin.profile.edit')}}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="card avatar">
            @if(isset($user) && $user->avatar !== null)
                <img src="{{ asset('storage/avatar/'.$user->avatar->filepath) }}" alt="" class="img-fluid">
            @else
                <img src="/img/Team-member.png" alt="" class="img-fluid">
            @endif
        </div>

        <div class="col card">
            <div class="card-body">
                <h2 class="h4"><i class="fas fa-fw fa-user"></i> Persoonlijke Gegevens</h2>
                <ul class="list-unstyled display-list">
                    <li>
                        <i class="far fa-fw fa-user"></i>
                        <span class="label medium">Volledige naam</span> :
                        {{isset($user->full_name)? $user->full_name : ''}}
                    </li>
                    <li>
                        <i class="far fa-fw fa-user"></i>
                        <span class="label medium">Voornaam</span> :
                        {{isset($user->first_name)? $user->first_name : ''}}
                    </li>
                    <li>
                        <i class="far fa-fw fa-user"></i>
                        <span class="label medium">Achternaam</span> :
                        {{isset($user->last_name)? $user->last_name : ''}}
                    </li>
                    <li>
                        <i class="far fa-fw fa-envelope"></i>
                        <span class="label medium">Email</span> :
                        {{isset($user->email)? $user->email : ''}}
                    </li>
                    <li>
                        <i class="far fa-fw fa-calendar-alt"></i>
                        <span class="label medium">Laatste login</span> :
                        {{ isset($user->last_login) ? $user->last_login->format('H:i d/m/Y') : '' }}
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-sm-12 col-xl-3 card">
            <div class="card-body">
                <h2 class="h4"><i class="fas fa-fw fa-home"></i> Adressen</h2>
                @include('shared.panels.home_address_details')
            </div>
        </div>
    </div>



    <div class="row mt-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="h4"><i class="fas fa-fw fa-home"></i> Statistieken</h2>
                </div>
                <div class="card-body">
                    @include('shared.panels.statistics_details')
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h2 class="h4"><i class="far fa-fw fa-calendar-alt"></i> Aanverwante adressen</h2>

                        <div class="btn-group-sm">
                        @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                            <a href="{{ route('admin.profile.address.new') }}" class="btn btn-sm btn-outline-dark">Nieuw Adres</a>
                        @elseif(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                            <a href="{{ route('admin.user.address.new', $user) }}" class="btn btn-sm btn-outline-dark">Nieuw Adres</a>
                        @endif
                        </div>
                    </div>

                </div>
                <div class="card-body table-responsive">
                    @include('shared.tables.addresses_widget')
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="h4"><i class="far fa-fw fa-calendar-alt"></i> Boekingen</h2>
                </div>
                <div class="card-body table-responsive">
                    @include('shared.tables.appointments_users')
                </div>
            </div>
        </div>
    </div>
@endsection
