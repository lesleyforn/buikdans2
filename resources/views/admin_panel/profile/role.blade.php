@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin panel</a></li>
            @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.users') }}" >Gebruikers</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.user.detail' , $user) }}" >Profiel</a></li>
            @else
                <li class="breadcrumb-item"><a href="{{ route('admin.profile') }}">Profiel</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">Rechten aanpassen</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="h4">Rechten aanpassen</h1>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                @include('shared.forms.roles')
            </div>
        </div>
    </div>
@endsection
