@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin panel</a></li>
        @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
            <li class="breadcrumb-item"><a href="{{ route('admin.users') }}" >Gebruikers</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.user.detail' , $user) }}" >Profiel</a></li>
        @else
            <li class="breadcrumb-item"><a href="{{ route('admin.profile') }}">Profiel</a></li>

        @endif
            <li class="breadcrumb-item active" aria-current="page">Aanpassen</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="h4">Account Gegevens</h1>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                @include('shared.forms.edit_profile')
            </div>
        </div>

        <div class="col-sm-12 mt-3">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="h4">Adres Gegevens</h1>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                @include('shared.forms.edit_home_address')
            </div>
        </div>
        @if(!Request::is('admin/profile/') || !Request::is('admin/profile') || !Request::is('admin/profile/*'))
            <div class="col-sm-12 mt-3">
                <div class="card card-header">
                    <div class="d-flex justify-content-between">
                        <h1 class="h4">Delete Mijn Account</h1>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 offset-md-2">
                            <p class="text-center">
                                Als u wilt dat uw account verwijderd word, wat inhoud: dat u word uitgelogt en niet meer kan inloggen met dit account
                                en uw gegevens worden verwijdert, klik dan op "Verwijderen".
                            </p>

                            <div class="text-right">
                                <form action="{{route('users.destroy' , $user)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-outline-danger">
                                        {{ __('Verwijderen') }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
