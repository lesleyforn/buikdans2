@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin Panel</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.appointments') }}" >Boekingen</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inbox</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12 mb-3">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h2 class="h4"><i class="far fa-calendar"></i> Kalender</h2>
                    </div>
                </div>
                <div class="card-body">
                    @include('shared.tables.calendar')
                </div>
            </div>
        </div>
    </div>
@endsection
