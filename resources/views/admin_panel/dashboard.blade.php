@extends('layouts.admin')

@section('breadcrumb')
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page" >Admin panel</li>
            </ol>
        </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card bg-primary text-white o-hidden">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fa fa-fw fa-comments"></i>
                    </div>
                    <p class="h5 mr-5">0 Nieuwe berichten</p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card bg-warning text-white o-hidden">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="far fa-calendar-check"></i>
                    </div>
                    <p class="h5 mr-5">0 Nieuwe Boekingen</p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card bg-success text-white o-hidden">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="far fa-calendar-check"></i>
                    </div>
                    <p class="h5 mr-5">0 Afwachtende Boekingen</p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card bg-danger text-white o-hidden">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="far fa-envelope"></i>
                    </div>
                    <p class="h5 mr-5">0 Nieuwe Leads</p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
    </div>
@endsection
