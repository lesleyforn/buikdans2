<nav class="sidebar">
    <!-- Sidebar Links -->
    <ul class="list-unstyled">
        <li class="{{ Request::is('admin') ? 'active' : '' }}">
            <a href="{{route('admin.dashboard')}}">
                <i class="fas fa-fw fa-chart-line"></i>
                <span class="label">Dashboard</span>
            </a>
        </li>
        <li class="{{ (Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*') ? 'active' : '') }}">
            <a href="{{route('admin.profile')}}">
                <i class="fas fa-fw fa-user-circle"></i>
                <span class="label">Profiel</span>
            </a>
        </li>
        <li class="{{ (Request::is('admin/mailbox/') || Request::is('admin/mailbox') || Request::is('admin/mailbox/*') ? 'active' : '') }}">
            <a href="#messagesSubMenu" data-toggle="collapse" aria-expanded="{{ (Request::is('admin/mailbox/') || Request::is('admin/mailbox') || Request::is('admin/mailbox/*') ? 'true' : 'false') }}">
                <i class="fas fa-fw fa-envelope"></i>
                <span class="label">Berichten</span>
            </a>
            <ul class="{{ (Request::is('admin/mailbox/') || Request::is('admin/mailbox') || Request::is('admin/mailbox/*') ? 'show' : '') }} collapse list-unstyled" id="messagesSubMenu">
                <li><a href="{{ route('admin.mailbox.new') }}">Nieuw</a></li>
                <li><a href="{{ route('admin.inbox') }}">Inbox</a></li>
                <li><a href="{{ route('admin.mailbox.history') }}">Verzonden</a></li>
            </ul>
        </li>
        <li class="{{ (Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*') || Request::is('admin/address/') || Request::is('admin/address') || Request::is('admin/address/*') ? 'active' : '') }}">
            <a href="{{route('admin.addresses')}}">
                <i class="fas fa-fw fa-road"></i>
                <span class="label">Adressen</span>
            </a>
        </li>
    @if(Auth::user()->hasAnyRole(['editor','admin']))
        <li class="{{ (Request::is('admin/appointment/') || Request::is('admin/appointment') || Request::is('admin/appointment/*') || Request::is('admin/appointments/') || Request::is('admin/appointments')|| Request::is('admin/appointments/*') ? 'active' : '') }}">
            <a href="#appointmentsSubMenu" data-toggle="collapse" aria-expanded="{{ (Request::is('admin/appointment/') || Request::is('admin/appointment') || Request::is('admin/appointment/*') || Request::is('admin/appointments/') || Request::is('admin/appointments')|| Request::is('admin/appointments/*') ? 'true' : 'false') }}">
                <i class="fas fa-fw fa-calendar-check"></i>
                <span class="label">Boekingen</span>
            </a>
            <ul class="{{ (Request::is('admin/appointment/') || Request::is('admin/appointment') || Request::is('admin/appointment/*') || Request::is('admin/appointments/') || Request::is('admin/appointments')|| Request::is('admin/appointments/*') ? 'show' : '') }} collapse list-unstyled" id="appointmentsSubMenu">
                <li><a href="{{ route('admin.appointments') }}">Kalender</a></li>
                <li><a href="{{ route('admin.appointments.pending') }}">In Afwachting</a></li>
                <li><a href="{{ route('admin.appointments.history') }}">Geschiedenis</a></li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->hasAnyRole(['admin']))
        <li class="{{ (Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*') || Request::is('admin/users/') || Request::is('admin/users') || Request::is('admin/users/*') ? 'active' : ' ') }}">
            <a href="{{ route('admin.users') }}">
                <i class="fas fa-fw fa-users"></i>
                <span class="label">Gebruikers</span>
            </a>
        </li>
    @endif
    </ul>

    <ul class="list-unstyled bottom">
        <li>
            <a class="sidebarCollapse">
                <i class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </i>
                <span class="label">Menu Verbergen</span>
            </a>
        </li>
    </ul>
</nav>
