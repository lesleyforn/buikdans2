@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin panel</a></li>
            <li class="breadcrumb-item active" aria-current="page">Adressen</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h2 class="h4"><i class="fas fa-fw fa-home"></i>Alle Geregistreerde Adressen</h2>

                    </div>
                </div>
                <div class="card-body">
                    @include('shared.tables.addresses')
                </div>
            </div>
        </div>
    </div>
@endsection
