@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin panel</a></li>

            @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.profile') }}" >Profiel</a></li>
            @elseif(Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*') || Request::is('admin/address/') || Request::is('admin/address') || Request::is('admin/address/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.addresses') }}" >Adressen</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.address.detail', $address) }}">Adres Details</a></li>
            @elseif(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.users') }}" >Gebruikers</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.user.detail', $user) }}" >Profiel</a></li>
            @endif
            @if(isset($address))
                <li class="breadcrumb-item active" aria-current="page">Adres Aanpassen</li>
            @else
                <li class="breadcrumb-item active" aria-current="page">Nieuw Adres</li>
            @endif
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-header">
                <div class="d-flex justify-content-between">
                    @if(isset($address))
                        <h1 class="h4">Adres Aanpassen</h1>
                    @else
                        <h1 class="h4">Nieuw Adres Toevoegen</h1>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                @include('shared.forms.address')
            </div>
        </div>
    </div>
@endsection
