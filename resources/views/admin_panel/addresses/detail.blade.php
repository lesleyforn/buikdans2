@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" >Admin panel</a></li>
            @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.profile') }}" >Profiel</a></li>
            @elseif(Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*') || Request::is('admin/address/') || Request::is('admin/address') || Request::is('admin/address/*'))
                <li class="breadcrumb-item"><a href="{{ route('admin.addresses') }}" >Adressen</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">Adres Details</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h2 class="h4"><i class="fas fa-fw fa-home"></i> Adres Details</h2>
                        @if($address->is_active > 0)
                        <form action="{{ isset($user) ? route('address.destroy', [$user, $address]) : route('address.destroy' , $address)}}" method="post" class="btn-group-sm">
                            @csrf
                            @method('DELETE')
                            @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                                <a href="{{ route('admin.profile.address.edit' , $address) }}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                            @elseif(Request::is('admin/address/') || Request::is('admin/address') || Request::is('admin/address/*'))
                                <a href="{{ route('admin.address.edit' , $address) }}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                            @elseif(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                                <a href="{{ route('admin.user.address.edit' , $address) }}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                            @endif

                            <button type="submit" class="btn btn-sm btn-outline-danger">Verwijderen</button>
                        </form>
                        @endif
                    </div>
                </div>
                <div class="card-body">

                    @include('shared.panels.address_details')


                    <div class="d-flex mt-3">
                        @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                            <a href="{{ route('admin.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                        @elseif(Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*'))
                            <a href="{{ route('admin.addresses') }}" class="btn btn-sm btn-outline-dark">Ga Terug</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
