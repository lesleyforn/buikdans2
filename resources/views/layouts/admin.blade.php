<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/admin.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/admin_panel.css') }}" rel="stylesheet">


    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
</head>
<body>
    <header id="page-header">
        <!-- Navbar -->
        @include('admin_panel.nav.nav-bar')
    </header>


    <div class="wrapper">
        <!-- Sidebar -->
        @include('admin_panel.nav.side-bar')


        <!-- Content -->
        <div id="content">
            <a type="button" class="sidebarCollapse">
                <i class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </i>
            </a>

            <div class="container-fluid pt-3">
                @yield('breadcrumb')

                @if (session('status'))
                    <div class="alert alert-success mb-3">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success mb-3">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('failed'))
                    <div class="alert alert-danger mb-3">
                        {{ session('failed') }}
                    </div>
                @endif
            </div>

            <main class="container-fluid">
                @yield('content')
            </main>


            <footer class="sticky-footer">
                <div class="container-fluid text-right">
                    <small>Copyright © Yasmien Oriental 2018</small>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
