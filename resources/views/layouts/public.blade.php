<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/blazy/latest/blazy.min.js"></script>
    <script src="{{ asset('js/public.js') }}" defer></script>
    <script src="{{ asset('js/modernizr.js') }}" defer></script>
    <script src="{{ asset('js/slick.js') }}" defer></script>
    @if(Request::is('/'))
        <script src="{{ asset('js/section-scroller.js') }}" defer></script>
    @endif

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/public.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>
    @include('public.nav.nav-bar')

    @yield('header')

    @yield('reservation')

    @yield('calendar')

    @yield('extra')

    @yield('about')

    @yield('photos')

    @yield('contact')

    @yield('content')

    @include('public.footer')

    @yield('models')
</body>
</html>
