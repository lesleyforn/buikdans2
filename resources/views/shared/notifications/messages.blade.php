@if (session('status'))
    <div class="col-sm-12">
        <div class="alert alert-success mb-3">
            {{ session('status') }}
        </div>
    </div>
@endif

@if (session('success'))
    <div class="col-sm-12">
        <div class="alert alert-success mb-3">
            {{ session('success') }}
        </div>
    </div>
@endif

@if (session('failed'))
    <div class="col-sm-12">
        <div class="alert alert-danger mb-3">
            {{ session('failed') }}
        </div>
    </div>
@endif
