<table class="table table-sm table-hover">
    <thead>
    <tr>
        <th scope="col" width="70">Type</th>
        <th scope="col">Adres</th>
        <th scope="col" width="30" class="text-center">#</th>
    </tr>
    </thead>
    <tbody>
    @foreach($user->addresses->where('address_type' ,'!=' , 'Home') as $address)

    @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
        <tr class='clickable-row' data-href='{{  route('admin.user.address.detail', $address) }}'>
    @elseif(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
        <tr class='clickable-row' data-href='{{  route('admin.profile.address.detail', $address->id) }}'>
    @else
        <tr class='clickable-row' data-href='{{  route('user.profile.address.detail', $address->id) }}'>
    @endif()
            <td>{{ $address->address_type }}</td>
            <td>{{ $address->line_1 }}</td>
            <td class="text-center">{{ $address->line_2 }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
