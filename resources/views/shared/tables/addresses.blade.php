<table class="table table-sm table-hover">
    <thead>
    <tr>
        <th scope="col" width="200">Gebruiker</th>
        <th scope="col" width="70">Type</th>
        <th scope="col">Adres</th>
        <th scope="col" width="30" class="text-center">#</th>
        <th scope="col" width="50" class="text-center">Verwijderd</th>
    </tr>
    </thead>
    <tbody>
    @foreach($addresses as $address)
        <tr class="clickable-row" data-href="{{ route('admin.address.detail', $address) }}">
            <td>{{ ucfirst($users->where('id', '=', $address->created_by_user_id)->first()->full_name) }}</td>
            <td>{{ ucfirst($address->address_type) }}</td>
            <td>{{ ucfirst($address->line_1) }}</td>
            <td class="text-center">{{ $address->line_2 }}</td>
            <td class="text-center">
                @if($address->is_active === 0)
                    <i class="fas fa-fw fa-check"></i>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
