<table class="table table-sm table-hover">
    <thead>
    <tr>
        <th scope="col" width="50">#</th>
        <th scope="col">Rechten</th>
        <th scope="col">Voornaam</th>
        <th scope="col">Achternaam</th>
        <th scope="col">E-mail</th>
        <th scope="col"  width="50">Actief</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr class='clickable-row' data-href='{{ route('admin.user.detail', $user->id) }}'>
            <td>{{ isset($user->id) ? $user->id : '' }}</td>
            <td>
                   {{ucfirst($user->roles->last()->name)}}
            </td>
            <td>{{ isset($user->first_name) ? ucfirst($user->first_name) : '' }}</td>
            <td>{{ isset($user->last_name) ? ucfirst($user->last_name) : '' }}</td>
            <td>{{ isset($user->email) ? $user->email : '' }}</td>
            <td class="text-center">
                @if(isset($user->is_active) && $user->is_active > 0)
                    <i class="fas fa-fw fa-check"></i>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
