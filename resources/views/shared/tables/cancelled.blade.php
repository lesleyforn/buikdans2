@if($appointments !== null)
    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col" width="50">#</th>
            <th scope="col" width="200">Status</th>
            <th scope="col">Event Titel</th>
            <th scope="col" width="200">Start Datum</th>
            <th scope="col" width="200">Aantal Shows</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($appointments as $appointment)
            @if($appointment->status()->first()->name === 'cancelled')
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $appointment->status()->first()->name }}</td>
                    <td>{{ $appointment->event_title }}</td>
                    <td>{{ $appointment->start_time }}</td>
                    <td>{{ $appointment->end_time_exp }}</td>
                    <td></td>
                </tr>
            @endif

            @if($loop->iteration < 1)
                <tr>
                    <td colspan="5"><p>Geen nieuwe afspraken beschikbaar.</p></td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@else
    <p>Er zijn momenteel geen nieuwe afspraken die goedkeuring vereisen.</p>
@endif
