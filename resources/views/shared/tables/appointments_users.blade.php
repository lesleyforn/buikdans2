
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col" width="50">#</th>
        <th scope="col" width="200">Status</th>
        <th scope="col">Event Titel</th>
        <th scope="col" width="200">Start Datum</th>
        <th scope="col" width="200">Aantal Shows</th>
    </tr>
    </thead>
    <tbody>
    @foreach($appointments as $appointment)
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{$appointment->status()->first()->name}}</td>
            <td>{{ $appointment->event_title }}</td>
            <td>{{ $appointment->start_time }}</td>
            <td>{{ $appointment->end_time_exp }}</td>
        </tr>
        @if($loop->iteration < 1)
            <tr>
                <td colspan="5"><p>Geen nieuwe afspraken beschikbaar.</p></td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>

