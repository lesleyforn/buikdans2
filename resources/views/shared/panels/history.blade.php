<div class="tab-content" id="tabContent">

    <div class="tab-pane fade show active" id="default" role="tabpanel" aria-labelledby="default-tab">
        <div class="card">
            <nav class="card-header d-flex justify-content-between">
                <h2 class="h4">Berichten</h2>
            </nav>
            <div class="card-body">
                <p>Geen bericht geselecteerd</p>
            </div>
        </div>
    </div>

    @isset($messages)
        @foreach($messages as $message)
            <div class="tab-pane fade" id="message-{{$loop->iteration}}" role="tabpanel" aria-labelledby="message-tab">
                <div class="card">
                    <nav class="card-header d-flex justify-content-between">
                        <h2 class="h4">Berichten</h2>
                        <form action="{{ route('message.destroy', $message) }}" class="btn-group-sm float-right" method="post">
                            @csrf
                            @method('DELETE')
                            {{--<a href="" class="btn btn-outline-dark" role="button">Reply</a>--}}
                            <button class="btn btn-outline-danger">Verwijderen</button>
                        </form>
                    </nav>
                    <header class="card-body">
                        <div class="d-flex flex-row justify-content-around no-gutters">
                            <div class="col">Ontvanger: {{$users->where('id', $message->creator_user_id)->first()->full_name}}</div>

                            <div class="col-auto">
                                <label class="mb-0">{{date('H:i d-m-Y', strtotime($message->created_at))}}</label>
                            </div>
                        </div>
                        <div class="d-flex no-gutters">
                            <div class="col-sm-12">Onderwerp: {{$message->subject}}</div>
                        </div>
                    </header>
                    <hr>
                    <main class="card-body">
                        <p>{{ $message->message_body }}</p>
                    </main>
                </div>
            </div>
        @endforeach
    @endisset
</div>
