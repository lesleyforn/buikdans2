<div class="row">
    <div class="col-sm-12">
        <ul class="list-unstyled display-list">
            <li>
                <i class="fas fa-fw fa-check"></i>
                <span class="label large">Actief</span> :
                {{isset($user->is_active)? ($user->is_active == 0 ? 'False' : 'True') : ''}}
            </li>
            <li>
                <i class="far fa-fw fa-calendar-alt"></i>
                <span class="label large">Aangemaakt op</span> :
                {{isset($user->created_at)? date('d-m-Y', strtotime($user->created_at))  : ''}}
            </li>
            <li>
                <i class="fas fa-fw fa-sign-in-alt"></i>
                <span class="label large">Totaal aantal login</span> : {{ isset($user->login_counter) ? $user->login_counter : 'error' }}
            </li>
            <li>
                <i class="far fa-fw fa-envelope-open"></i>
                <span class="label large">Totaal aantal berichten</span> : 0
            </li>
            <li>
                <i class="far fa-fw fa-calendar"></i>
                <span class="label large">Totaal aantal boekingen</span> : 0
            </li>
        </ul>
    </div>
</div>
