<div class="row">
    <div class="col-sm-12">
        <ul class="list-unstyled display-list">
            @if($address->is_active === 0)
                <li>
                    <h3>Dit adres is verwijderd!</h3>
                </li>
            @endif
        @if(Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*'))

            <li>
                <i class="far fa-fw fa-user"></i>
                <span class="label large">Gebruiker</span> :
                <a href="{{ route('admin.address.detail', $address->created_by_user_id) }}" class="value">
                    {{ isset($address->created_by_user_id) ? $users->where('id', '=', $address->created_by_user_id)->first()->full_name : '' }}
                </a>
            </li>
        @endif
            <li>
                <i class="far fa-fw fa-address-book"></i>
                <span class="label large">Adres type</span> :
                <span class="value"> {{ isset($address->address_type) ? ucfirst($address->address_type) : '' }}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-road"></i>
                <span class="label large">Straat</span> :
                <span class="value"> {{ isset($address->line_1) ? ucfirst($address->line_1) : '' }}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-tag"></i>
                <span class="label large">Huisnummer</span> :
                <span class="value"> {{isset($address->line_2) ? $address->line_2 : ''}}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-plus"></i>
                <span class="label large">Extra informatie</span> :
                <span class="value"> {{isset($address->line_3) ? ucfirst($address->line_3) : ''}}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-sort-numeric-up"></i>
                <span class="label large">Postcode</span> :
                <span class="value"> {{ isset($address->zip_code) ? $address->zip_code : '' }}</span>
            </li>
            <li>
                <i class="far fa-fw fa-building"></i>
                <span class="label large">Stad</span> :
                <span class="value"> {{isset($address->city) ? ucfirst($address->city) : ''}}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-globe"></i>
                <span class="label large">Land</span> :
                <span class="value"> {{ ucfirst($address->countries->first()->name) }}</span>
            </li>
            <li>

                <p class="mb-0">
                    <i class="far fa-fw fa-building"></i> <span class="label large">Extra Info</span> :
                    {{isset($address->misc) ? ucfirst($address->misc) : ''}}
                </p>
            </li>
        </ul>
    </div>
</div>
