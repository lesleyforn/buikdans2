<div class="row">
    <div class="col-sm-12">
        <ul class="list-unstyled display-list">
            <li>
                <i class="fas fa-fw fa-road"></i>
                <span class="label">Straat</span> :
                <span class="value"> {{ isset($homeAddress->line_1) ? ucfirst($homeAddress->line_1) : '' }}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-tag"></i>
                <span class="label">Huisnummer</span> :
                <span class="value"> {{isset($homeAddress->line_2) ? ucfirst($homeAddress->line_2) : ''}}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-sort-numeric-up"></i>
                <span class="label">Postcode</span> :
                <span class="value"> {{ isset($homeAddress->zip_code) ? $homeAddress->zip_code : '' }}</span>
            </li>
            <li>
                <i class="far fa-fw fa-building"></i>
                <span class="label">Stad</span> :
                <span class="value"> {{isset($homeAddress->city) ? ucfirst($homeAddress->city) : ''}}</span>
            </li>
            <li>
                <i class="fas fa-fw fa-globe"></i>
                <span class="label">Land</span> :
                <span class="value"> {{ ucfirst($homeAddress->countries->first()->name) }}</span>
            </li>
        </ul>
    </div>
</div>
