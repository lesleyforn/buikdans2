<form method="POST" action="{{ route('appointment.store') }}">
    @csrf

    <div class="form-group row">
        <label for="event_title" class="col-sm-12 col-form-label">{{ __('Event Titel:') }}</label>

        <div class="col">
            <input id="event_title" type="text" class="form-control{{ $errors->has('event_title') ? ' is-invalid' : '' }}" name="event_title" value="{{ old('event_title') }}" required>

            @if ($errors->has('event_title'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('event_title') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="start_date" class="col-sm-12 col-form-label">{{ __('Event datum:') }}</label>

        <div class="col">
            <input id="start_date" type="date" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}" name="start_date" value="{{ old('start_date') }}" required>

            @if ($errors->has('start_date'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('start_date') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="start_time" class="col-sm-12 col-form-label">{{ __('Event start uur:') }}</label>

        <div class="col">
            <input id="start_time" type="time" class="form-control{{ $errors->has('start_time') ? ' is-invalid' : '' }}" name="start_time" value="{{ old('start_time') }}" required>

            @if ($errors->has('start_time'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('start_time') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="end_time_exp" class="col-sm-12 col-form-label">{{ __('Aantal optredens per show:') }}</label>

        <div class="col">
            <input id="end_time_exp" type="number" step="1" class="form-control{{ $errors->has('end_time_exp') ? ' is-invalid' : '' }}" name="end_time_exp" value="{{ old('end_time_exp') }}" required>

            @if ($errors->has('end_time_exp'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('end_time_exp') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="description" class="col-sm-12 col-form-label">{{ __('Event Beschrijving') }} :</label>

        <div class="col">
            <textarea name="description" id="message_body" rows="8" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}">{{ old('description') }}</textarea>

            @if ($errors->has('description'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col">
            <div class="d-flex justify-content-end no-gutters">

                <button type="submit" class="btn btn-primary">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>

</form>
