<form class="form-horizontal" method="POST" action="{{ route('users.change.password', $user) }}">
    @csrf
    @method('PUT')
    <div class="form-group row {{ $errors->has('current-password') ? ' has-error' : '' }}">
        <label for="new-password" class="col-md-4 col-xl-2 col-form-label text-md-right">Huidige Wachtwoord</label>

        <div class="col-md-6 col-xl-8">
            <input id="current-password" type="password" class="form-control" name="current-password" required>

            @if ($errors->has('current-password'))
                <span class="help-block">
                    <strong>{{ $errors->first('current-password') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="form-group row {{ $errors->has('new-password') ? ' has-error' : '' }}">
        <label for="new-password" class="col-md-4 col-xl-2 col-form-label text-md-right">Nieuwe Wachtwoord</label>

        <div class="col-md-6 col-xl-8">
            <input id="new-password" type="password" class="form-control" name="new-password" required>

            @if ($errors->has('new-password'))
            <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="new-password-confirm" class="col-md-4 col-xl-2 col-form-label text-md-right">Bevestig Nieuwe Wachtwoord</label>

        <div class="col-md-6 col-xl-8">
            <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2">
            <div class="d-flex justify-content-between no-gutters">

                @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                    <a href="{{ route('admin.user.detail', $user) }}" class="btn btn-outline-dark">Ga Terug</a>
                @elseif(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                    <a href="{{ route('admin.profile.edit') }}" class="btn btn-outline-dark">Ga Terug</a>
                @else
                    <a href="{{ route('user.profile.edit') }}" class="btn btn-outline-dark">Ga Terug</a>
                @endif

                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>
</form>
