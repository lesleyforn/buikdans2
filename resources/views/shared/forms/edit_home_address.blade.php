<form method="POST" action="{{ route('address.update' , $homeAddress) }}">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="line_1" class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Adres') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="line_1" type="text" class="form-control{{ $errors->has('line_1') ? ' is-invalid' : '' }}" name="line_1" value="{{ old('line_1') != null ? old('line_1') : (isset($homeAddress->line_1) ? $homeAddress->line_1 : '') }}" required>

            @if ($errors->has('line_1'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('line_1') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="line_2" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Huisnummer') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="line_2" type="text" class="form-control{{ $errors->has('line_2') ? ' is-invalid' : '' }}" name="line_2" value="{{ old('line_2') != null ? old('line_2') : (isset($homeAddress->line_2) ? $homeAddress->line_2 : '') }}" required>

            @if ($errors->has('line_2'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('line_2') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="zip_code" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Postcode') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{ old('zip_code') != null ? old('zip_code') : (isset($homeAddress->zip_code) ? $homeAddress->zip_code : '') }}" required>

            @if ($errors->has('zip_code'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('zip_code') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="city" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Stad') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') != null ? old('city') : (isset($homeAddress->city) ? $homeAddress->city : '') }}" required>

            @if ($errors->has('zip_code'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('zip_code') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="country" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Land') }}</label>

        <div class="col-md-6 col-xl-8">

            <select id="country" class="form-control" name="country">
                @foreach( $countries as $country)
                <option value="{{ $country->id }}" {{ $country->id === $homeAddress->countries->first()->id ? 'selected' : '' }}>{{ $country->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('country'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="form-group row mb-0 pt-3">
        <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2">
            <div class="d-flex justify-content-between no-gutters">
                @if(Request::is('admin/profile/') || Request::is('admin/profile') || Request::is('admin/profile/*'))
                    <a href="{{ route('admin.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                @elseif(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                    <a href="{{ route('admin.user.detail', $user) }}" class="btn btn-outline-dark">Ga Terug</a>
                @elseif(Request::is('user/profile/') || Request::is('user/profile') || Request::is('user/profile/*'))
                    <a href="{{ route('user.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                @endif

                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>
</form>
