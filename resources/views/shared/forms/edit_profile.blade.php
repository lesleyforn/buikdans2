
    <form method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="first_name" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Voornaam') }}</label>

            <div class="col-md-6 col-xl-8">
                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') != null ? old('first_name') : (isset($user->first_name) ? $user->first_name : '') }}" required>

                @if ($errors->has('first_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="last_name" class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Achternaam') }}</label>

            <div class="col-md-6 col-xl-8">
                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') != null ? old('last_name') : (isset($user->last_name) ? $user->last_name : '') }}" required>

                @if ($errors->has('last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Wachtwoord') }}</label>

            <div class="col-md-6 col-xl-8">
                <a href="password/change" class="form-control-plaintext">Change Password</a>
            </div>
        </div>

        <div class="form-group row">
            <label for="avatar" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Avatar Aanpassen') }}</label>

            <div class="col-md-6 col-xl-8">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-auto pr-2 pb-2">
                        @if(isset($user) && $user->avatar !== null)
                            <img src="{{ asset('storage/avatar/'.$user->avatar->filepath) }}" alt="" class="img-fluid" width="150">
                        @else
                            <img src="/img/Team-member.png" alt="" class="img-fluid" width="150">
                        @endif
                    </div>
                    <div class="col">
                        <input id="avatar" type="file" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" name="avatar" value="{{ old('avatar') }}">
                    </div>
                </div>


                @if ($errors->has('avatar'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row mb-0 mt-3">
            <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2 text-right">
                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </form>
