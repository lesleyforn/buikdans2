<form method="POST" action="{{ route('message.store') }}">
    @csrf

    @if(Request::is('admin/mailbox/') || Request::is('admin/mailbox') || Request::is('admin/mailbox/*'))
    <div class="form-group row">
        <label for="to" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Naar') }} :</label>

        <div class="col-md-6 col-xl-8">
            <input id="to" list="users_full_name" class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" name="full_name" value="{{ old('to') }}">

            @if ($errors->has('to'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('to') }}</strong>
                </span>
            @endif

            <datalist id="users_full_name">
                @foreach($users as $user)
                    @if($user->id !== Auth::user()->id)
                        <option value="{{ $user->full_name }}">
                    @endif
                @endforeach
            </datalist>
        </div>
    </div>
    @endif

    <div class="form-group row">
        <label for="subject" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Onderwerp') }} :</label>

        <div class="col-md-6 col-xl-8">
            <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject') }}">

            @if ($errors->has('subject'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="message_body" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Bericht') }} :</label>

        <div class="col-md-6 col-xl-8">
            <textarea name="message_body" id="message_body" cols="30" rows="10" class="form-control{{ $errors->has('message_body') ? ' is-invalid' : '' }}">{{ old('message_body') }}</textarea>

            @if ($errors->has('message_body'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('message_body') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2">
            <div class="d-flex justify-content-between no-gutters">
                @if(Request::is('admin/') || Request::is('admin') || Request::is('admin/*'))
                    <a href="{{ route('admin.inbox') }}" class="btn btn-outline-dark">Ga Terug</a>
                @elseif(Request::is('user/') || Request::is('user') || Request::is('user/*'))
                    <a href="{{ route('user.inbox') }}" class="btn btn-outline-dark">Ga Terug</a>
                @endif

                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>
</form>
