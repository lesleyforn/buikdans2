<form method="POST" action="{{ route('users.change.role', $user) }}">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="role" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Role') }} :</label>

        <div class="col-md-6 col-xl-8">
            <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" id="role" name="role">
                @foreach($roles as $role)
                    <option value="{{$role->id}}" @if($user->roles->last()->name === $role->name) selected @endif> {{ucfirst($role->name)}}</option>
                @endforeach
            </select>
            @if ($errors->has('role'))
                <span class="invalid-feedback">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row mb-0 pt-3">
        <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2">
            <div class="d-flex justify-content-between no-gutters">

                <a href="{{ route('admin.user.detail', $user) }}" class="btn btn-outline-dark">Ga Terug</a>

                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>
</form>
