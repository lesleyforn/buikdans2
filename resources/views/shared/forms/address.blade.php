

<form method="POST" action="{{ isset($address) ? route('address.update', $address) :  route('address.store') }}">
    @csrf

    @if(isset($address))
        @method('PUT')
    @endif

    <div class="form-group row">
        <label class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Adres Type ') }}</label>
        <div class="col-md-6 col-xl-8 form-control-plaintext">
            @if(Request::is('admin/addresses/') || Request::is('admin/addresses') || Request::is('admin/addresses/*'))
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="address_type" id="inlineRadioCommunity" value="Home" {{ isset($address) ? ($address->address_type === 'Home' ?  'checked' : '' ) : (old('address_type') === 'Home' ? 'checked' : '') }} required>
                <label class="form-check-label" for="inlineRadioCommunity">Thuis Adres</label>
            </div>
            @endif
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="address_type" id="inlineRadioCommunity" value="Community" {{ isset($address) ? ($address->address_type === 'Community' ?  'checked' : '' ) : (old('address_type') === 'Community' ? 'checked' : '') }} required>
                <label class="form-check-label" for="inlineRadioCommunity">Gemeendschapshuis</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="address_type" id="inlineRadioEvent" value="Event" {{ isset($address) ? ($address->address_type === 'Event' ?  'checked' : '' ) : (old('address_type') === 'Event' ? 'checked' : '') }} required>
                <label class="form-check-label" for="inlineRadioEvent">Event locatie</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="address_type" id="inlineRadioRestaurant" value="Restaurant" {{ isset($address) ? ($address->address_type === 'Restaurant' ?  'checked' : '' ) : (old('address_type') === 'Restaurant' ? 'checked' : '') }} required>
                <label class="form-check-label" for="inlineRadioRestaurant">Restaurant</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="address_type" id="inlineRadioOther" value="Other" {{ isset($address) ? ($address->address_type === 'Other' ?  'checked' : '' ) : (old('address_type') === 'Other' ? 'checked' : '') }} required>
                <label class="form-check-label" for="inlineRadioOther">Anders</label>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="line_1" class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Straat ') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="line_1" type="text" class="form-control{{ $errors->has('line_1') ? ' is-invalid' : '' }}" name="line_1" value="{{ isset($address) ? $address->line_1 : old('line_1') }}" >

            @if ($errors->has('line_1'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('line_1') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="line_2" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Huisnummer') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="line_2" type="text" class="form-control{{ $errors->has('line_2') ? ' is-invalid' : '' }}" name="line_2" value="{{ isset($address) ? $address->line_2 : old('line_2') }}" required>

            @if ($errors->has('line_2'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('line_2') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="line_3" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Extra Adres gegevens') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="line_3" type="text" class="form-control{{ $errors->has('line_3') ? ' is-invalid' : '' }}" name="line_3" value="{{ isset($address) ? $address->line_3 : old('line_3') }}">

            @if ($errors->has('line_3'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('line_3') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="zip_code" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Postcode') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{ isset($address) ? $address->zip_code : old('zip_code') }}" required>

            @if ($errors->has('zip_code'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('zip_code') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="city" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Stad') }}</label>

        <div class="col-md-6 col-xl-8">
            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ isset($address) ? $address->city : old('city') }}" required>

            @if ($errors->has('zip_code'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('zip_code') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="country" class="col-md-4 col-xl-2  col-form-label text-md-right">{{ __('Land') }}</label>

        <div class="col-md-6 col-xl-8">

            <select id="country" class="form-control" name="country">
                @foreach( $countries as $country)
                    <option value="{{ $country->id }}" {{ $country->id !==  old('country') ? (isset($address) && $address->countries->first()->id === $country->id ? 'selected' : '') : 'selected' }}>{{ $country->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('country'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="misc" class="col-md-4 col-xl-2 col-form-label text-md-right">{{ __('Extra Informatie') }}</label>

        <div class="col-md-6 col-xl-8">
            <textarea name="misc" id="misc" cols="30" rows="10" class="form-control{{ $errors->has('misc') ? ' is-invalid' : '' }}">{{ isset($address) ? $address->misc : old('misc') }}</textarea>


            @if ($errors->has('misc'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('misc') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 col-xl-8 offset-md-4 offset-xl-2">
            <div class="d-flex justify-content-between no-gutters">

                    @if(Request::is('admin/user/') || Request::is('admin/user') || Request::is('admin/user/*'))
                        <a href="{{ route('admin.user.detail', $user) }}" class="btn btn-outline-dark">Ga Terug</a>
                    @elseif(Request::is('admin/profile/') || Request::is('admin/prof') || Request::is('admin/user/*'))
                        <a href="{{ route('admin.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                    @else
                        <a href="{{ route('user.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                    @endif

                <button type="submit" class="btn btn-outline-dark">
                    {{ __('Versturen') }}
                </button>
            </div>
        </div>
    </div>
</form>
