@extends('layouts.public')

@section('content')
    <main id="editMyProfile">
        <div class="row">
            @include('shared.notifications.messages')

            <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Gebruikers Panel</a></li>
                        @if(isset($address))
                            <li class="breadcrumb-item"><a href="{{ route('user.address.detail', $address) }}" >Adres Gegevens</a></li>
                        @endif
                        <li class="breadcrumb-item active" aria-current="page" >Adres details</li>
                    </ol>
                </nav>
            </div>

            <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                <div class="card card-header">
                    <div class="d-flex justify-content-between">
                        @if(isset($address))
                            <h1 class="h4">Adres Aanpassen</h1>
                        @else
                            <h1 class="h4">Nieuw Adres Toevoegen</h1>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                <div class="card card-body">
                    @include('shared.forms.address')
                </div>
            </div>
        </div>
    </main>
@endsection
