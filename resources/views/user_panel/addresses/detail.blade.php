@extends('layouts.public')

@section('content')
    <main id="editAddress">
        <div class="row">

            <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Gebruikers Panel</a></li>
                        <li class="breadcrumb-item active" aria-current="page" >Adres Gegevens</li>
                    </ol>
                </nav>
            </div>

            @include('shared.notifications.messages')

            <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h2 class="h4"><i class="fas fa-fw fa-home"></i> Adres Gegevens</h2>
                            @if($address->is_active > 0)
                                <form action="{{ isset($user) ? route('address.destroy', [$user, $address]) : route('address.destroy' , $address)}}" method="post" class="btn-group-sm">
                                    @csrf
                                    @method('DELETE')

                                    <a href="{{ route('user.profile.address.edit' , $address) }}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                                    <button type="submit" class="btn btn-sm btn-outline-danger">Verwijderen</button>

                                </form>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">

                        @include('shared.panels.address_details')

                        <div class="d-flex mt-3">
                            <a href="{{ route('user.profile') }}" class="btn btn-outline-dark">Ga Terug</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
