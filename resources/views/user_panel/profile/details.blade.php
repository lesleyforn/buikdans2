@extends('layouts.public')

@section('content')
    <main id="myProfile">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Profiel</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1">
                    <div class="card card-header">
                        <div class="d-flex justify-content-between">
                            <h1 class="h4">Profiel</h1>

                            <div class="btn-group-sm">
                                <a href="{{route('user.profile.edit')}}" class="btn btn-sm btn-outline-dark">Aanpassen</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1 pr-sm-0 pr-lg-3 row">
                    <div class="card avatar">
                        <img src="/img/Team-member.png" alt="">
                    </div>

                    <div class="col card">
                        <div class="card-body">
                            <h2 class="h4"><i class="fas fa-fw fa-user"></i> Persoonlijke Gegevens</h2>
                            <ul class="list-unstyled display-list">
                                <li>
                                    <i class="far fa-fw fa-user"></i>
                                    <span class="label medium">Volledige naam</span> :
                                    {{isset($user->full_name)? $user->full_name : ''}}
                                </li>
                                <li>
                                    <i class="far fa-fw fa-user"></i>
                                    <span class="label medium">Voornaam</span> :
                                    {{isset($user->first_name)? $user->first_name : ''}}
                                </li>
                                <li>
                                    <i class="far fa-fw fa-user"></i>
                                    <span class="label medium">Achternaam</span> :
                                    {{isset($user->last_name)? $user->last_name : ''}}
                                </li>
                                <li>
                                    <i class="far fa-fw fa-envelope"></i>
                                    <span class="label medium">E-mail</span> :
                                    {{isset($user->email)? $user->email : ''}}
                                </li>
                                <li>
                                    <i class="far fa-fw fa-calendar-alt"></i>
                                    <span class="label medium">Laatste login</span> :
                                    {{ isset($user->last_login) ? $user->last_login->format('H:i d/m/Y') : '' }}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-xl-3 card">
                        <div class="card-body">
                            <h2 class="h4"><i class="fas fa-fw fa-home"></i> Adres</h2>
                            @include('shared.panels.home_address_details')
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12 col-lg-10 col-lg-10 offset-lg-1 offset-xl-1 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="h4"><i class="fas fa-fw fa-home"></i> Statistieken</h2>
                        </div>
                        <div class="card-body">
                            @include('shared.panels.statistics_details')
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-sm-12 col-lg-10 offset-lg-1 offset-xl-1">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h2 class="h4"><i class="far fa-fw fa-calendar-alt"></i> Verwante adressen</h2>

                                <div class="btn-group-sm">
                                    <a href="{{ route('user.profile.address.new') }}" class="btn btn-sm btn-outline-dark">Nieuwe Toevoegen</a>
                                </div>
                            </div>

                        </div>
                        <div class="card-body table-responsive">
                            @include('shared.tables.addresses_widget')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
