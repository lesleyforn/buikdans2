@extends('layouts.public')

@section('content')
    <main id="editProfile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Profiel</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('user.profile.edit') }}" >Aanpassen</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Wachtwoord Aanpassen</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <div class="card card-header">
                        <div class="d-flex justify-content-between">
                            <h1 class="h4">Verander wachtwoord</h1>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <div class="card card-body">
                        @include('shared.forms.change_password')
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
