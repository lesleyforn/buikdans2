@extends('layouts.public')

@section('content')
    <main id="editProfile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Profiel</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Aanpassen</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <div class="card card-header">
                        <div class="d-flex justify-content-between">
                            <h1 class="h4">Account Gegevens</h1>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <div class="card card-body">
                        @include('shared.forms.edit_profile')
                    </div>
                </div>

                <div class="col-sm-12 mt-3 col-lg-8 offset-lg-2">
                    <div class="card card-header">
                        <div class="d-flex justify-content-between">
                            <h1 class="h4">Adres Gegevens</h1>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-8 offset-lg-2">
                    <div class="card card-body">
                        @include('shared.forms.edit_home_address')
                    </div>
                </div>
                @if(!Request::is('admin/profile/') || !Request::is('admin/profile') || !Request::is('admin/profile/*'))
                    <div class="col-sm-12 mt-3 col-lg-8 offset-lg-2">
                        <div class="card card-header">
                            <div class="d-flex justify-content-between">
                                <h1 class="h4">Verwijder Mijn Account</h1>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-8 offset-lg-2">
                        <div class="card card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-8 offset-md-2">
                                    <p class="text-center">
                                        Als u wilt dat uw account verwijderd word, wat inhoud: dat u word uitgelogt en niet meer kan inloggen met dit account
                                        en uw gegevens worden verwijdert, klik dan op "Verwijderen".
                                    </p>

                                    <div class="text-right">
                                        <a href="{{route('users.destroy', $user->id)}}" class="btn btn-outline-danger">
                                            {{ __('Delete') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </main>
@endsection
