@extends('layouts.public')

@section('content')
    <main id="inbox">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8 offset-lg-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Verzonden Berichten</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-lg-8 offset-lg-2 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h2 class="h4"><i class="far fa-fw fa-envelope"></i> Verzonden berichten</h2>
                                <div>
                                    <a href="{{route('user.mailbox.new')}}" class="btn btn-sm btn-outline-dark">Nieuw bericht</a>
                                    <a href="{{route('user.inbox')}}" class="btn btn-sm btn-outline-dark">Inbox</a>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            @if(isset($messages)&& $messages->first() !== null)
                                <ul class="nav flex-column list-unstyled display-list small" id="tab" role="tablist">
                                    <li>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <strong>Onderwerp</strong>
                                            </div>
                                            <strong>Tijd en Datum</strong>
                                        </div>
                                    </li>
                                    @foreach($messages as $message)
                                        <li>
                                            <a class="nav-link" data-toggle="pill" href="#message-{{$loop->iteration}}" aria-controls="message-{{$loop->iteration}}" aria-selected="true">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <span>{{isset($message->subject) ? $message->subject : ""}}</span>
                                                    </div>
                                                    <span>{{isset($message->created_at) ? $message->created_at : ""}}</span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <p>
                                    Geen nieuwe berichten.
                                </p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 offset-lg-2 mb-3">
                    @include('shared.panels.history')
                </div>

            </div>
        </div>
    </main>
@endsection
