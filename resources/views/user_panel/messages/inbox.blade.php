@extends('layouts.public')

@section('content')
    <main id="inbox">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8 offset-lg-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Inbox</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-lg-8 offset-lg-2 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h2 class="h4"><i class="far fa-fw fa-envelope"></i> Inbox</h2>
                                <div>
                                    <a href="{{route('user.mailbox.new')}}" class="btn btn-sm btn-outline-dark">Nieuw bericht</a>
                                    <a href="{{route('user.mailbox.history')}}" class="btn btn-sm btn-outline-dark">Verzonden berichten</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if(isset($messages)&& $messages->first() !== null)
                                <ul class="nav flex-column list-unstyled display-list small" id="tab" role="tablist">
                                        <li>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <strong class="read">Gelezen</strong>
                                                    <strong>Onderwerp</strong>
                                                </div>
                                                <strong>Tijd en Datum</strong>
                                            </div>
                                        </li>
                                    @foreach($messages as $message)
                                        <li>
                                            <a class="nav-link" data-toggle="pill" href="#message-{{$loop->iteration}}" aria-controls="message-{{$loop->iteration}}" aria-selected="true">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <span class="read">
                                                            @if(isset($message->is_read))
                                                                @if($message->is_read === 0)
                                                                    <i class="far fa-envelope"></i>
                                                                @else
                                                                    <i class="far fa-envelope-open"></i>
                                                                @endif
                                                            @endif
                                                        </span>
                                                        <span>{{isset($message->message->subject) ? $message->message->subject : ""}}</span>
                                                    </div>
                                                    <span>{{isset($message->message->created_at) ? $message->message->created_at : ""}}</span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <p>
                                    No new messages
                                </p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 offset-lg-2 mb-3">
                    @include('shared.panels.inbox')
                </div>

            </div>
        </div>
    </main>
@endsection
