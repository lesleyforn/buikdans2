@extends('layouts.public')

@section('content')
    <main id="inbox">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8 offset-lg-2 mb-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('user.inbox') }}" >Inbox</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Nieuw Bericht</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-lg-8 offset-lg-2 mb-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h2 class="h4">Nieuw Bericht</h2>
                            </div>
                        </div>

                        <div class="card-body">
                            @include('shared.forms.message')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
