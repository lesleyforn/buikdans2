@extends('layouts.public')

@section('breadcrumb')

@endsection

@section('content')
    <main id="myAppointments">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.profile') }}" >Admin Panel</a></li>
                            <li class="breadcrumb-item active" aria-current="page" >Boekingen</li>
                        </ol>
                    </nav>
                </div>

                @include('shared.notifications.messages')

                <div class="col-lg-3 mb-4 pr-lg-0">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="h4"><i class="far fa-fw fa-calendar-alt"></i> Nieuwe Boeking</h2>
                        </div>

                        <div class="card-body">
                            @include('shared.forms.new_appointment')
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 mb-4">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="h4"><i class="far fa-fw fa-calendar-alt"></i> Boekingen Status</h2>
                        </div>
                        <div class="card-body table-responsive">
                            @include('shared.tables.appointments_users')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
