@extends('layouts.public')

@section('header')
    <!-- Header -->
    <section id="page-top" class="masthead">
        <div class="container-fluid">
            <div class="intro-text">
                <h2 class="intro-lead-in">Welcome To Our Dance Studio!</h2>
                <h1 class="intro-heading text-uppercase">Oriental Dancer Yasmien</h1>
                {{--<a class="btn btn-primary rounded btn-xl text-uppercase js-scroll-trigger" href="#reservation">Boekings informatie</a>--}}
            </div>
        </div>
    </section>
@endsection

@section('reservation')
    <!-- Services -->
    <section id="reservation" class="large">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12 col-md-8 offset-md-2 text-center">
                    <h2 class="section-heading text-uppercase mt-5">Boekings Informatie</h2>
                    <div class="card-body mt-2">
                        <hr>
                        <p class="mt-5 mb-5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro ratione sed sit. Accusantium aperiam, dolor doloremque explicabo id, in ipsum laboriosam laudantium mollitia, neque quaerat suscipit veniam? Dolorum, fugit, vitae?
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid dicta dolor dolorem doloribus ex in iste iure mollitia necessitatibus porro, quidem similique veniam. Fugit illo ipsa iusto pariatur sunt.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur consequuntur est exercitationem fuga magnam, ut vero. Commodi ducimus eius eum id ipsam iure laborum mollitia, numquam porro sequi ut?
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam architecto cum debitis deleniti dolorum ducimus eligendi, eos expedita fugiat, id, impedit in ipsum libero nisi sapiente sunt totam voluptatibus!
                        </p>
                        <hr>
                    </div>
                    <a class="btn btn-primary btn-lg mb-5" data-toggle="modal" data-target="#registerModel" href="#registerModel">
                        Boek me nu!
                    </a>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('calendar')
    <!-- Services -->
    <section id="calendar" class="large">
        <div class="container-fluid no-padding">
            <div class="row no-gutters">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <h2 class="section-heading text-center text-uppercase intro-heading text-shadow">Aankomende Shows</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-lg-8">
                    <ul class="container-fluid shows pr-md-0">
                        <li class="row no-gutters">
                            <div class="col-auto pl-0 img-holder">
                                <img
                                    src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                    data-src="/images/calendar/temp1.jpg"
                                    alt="..." class="img-fluid b-lazy">
                            </div>

                            <div class="col">
                                <div class="heading">
                                    <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                                    <h4 class="media-heading">04 Juni</h4>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet perferendis sit sunt voluptatum!
                                </p>
                            </div>
                        </li>
                        <li class="row no-gutters">
                            <div class="col-auto pl-0" style="background: #000; display:flex; align-items: center;">
                                <img src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                    data-src="/images/calendar/temp2.jpg"
                                    alt="..." class="img-fluid b-lazy">
                            </div>

                            <div class="col">
                                <div class="heading">
                                    <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                                    <h4 class="media-heading">04 Juni</h4>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet perferendis sit sunt voluptatum!
                                </p>
                            </div>
                        </li>
                        <li class="row no-gutters">
                            <div class="col-auto pl-0" style="background: #000; display:flex; align-items: center;">
                                <img src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                    data-src="/images/calendar/temp3.jpg"
                                    alt="..." class="img-fluid b-lazy">
                            </div>

                            <div class="col">
                                <div class="heading">
                                    <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                                    <h4 class="media-heading">04 Juni</h4>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet perferendis sit sunt voluptatum!
                                </p>
                            </div>
                        </li>
                        <li class="row no-gutters">
                            <div class="col-auto pl-0" style="background: #000; display:flex; align-items: center;">
                                <img src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                     data-src="/images/calendar/temp4.jpg"
                                     alt="..." class="img-fluid b-lazy">
                            </div>

                            <div class="col">
                                <div class="heading">
                                    <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                                    <h4 class="media-heading">04 Juni</h4>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet perferendis sit sunt voluptatum!
                                </p>
                            </div>
                        </li>
                        <li class="row no-gutters">
                            <div class="col-auto pl-0" style="background: #000; display:flex; align-items: center;">
                                <img src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                     data-src="/images/calendar/temp5.jpg"
                                     alt="..." class="img-fluid b-lazy">
                            </div>

                            <div class="col">
                                <div class="heading">
                                    <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                                    <h4 class="media-heading">04 Juni</h4>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet perferendis sit sunt voluptatum!
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
@endsection


@section('extra')
    <!-- Intro -->
    <section id="extra" class="large">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-8 offset-md-2 text-center">
                    <h2 class="section-heading mt-5">Bent u op zoek naar dat tikkeltje extra voor uw feest?</h2>
                    <div class="card-body mt-lg-2">
                        <hr>
                        <p class="mt-lg-5">
                            Yasmien is een ervaren professioneel buikdanseres en buikdanslerares uit Belgisch limburg. In de Belgische buikdanswereld (en daarbuiten) is ze gekend als een sierlijke danseres met haar eigen elegante stijl. Haar optredens en die van haar wedstrijd- en showgroep Indirah zijn steeds smaakvol en geschikt voor jong en oud.
                        </p>
                        <br>
                        <h4 class="mb-3">Ook om te leren buikdansen kan je bij Yasmien terecht!</h4>
                        <p class="mb-lg-5">
                            Al vele jaren geeft ze ook wekelijkse lessen op verschillende locaties in Belgisch limburg, voor zowel beginners tot gevorderden.<br>
                            Haar leerlingen kennen haar als een enthousiast lerares die met veel geduld al haar kennis overdraagt.
                            Ook in de buikdansgemeenschap draagt ze haar steentje bij. Zo organiseert ze tal van evenementen voor beginnende tot gevorderde danseressen, gaande van workshops en shows, tot het jaarlijkse buikdansfestival Belly 2 Belly.
                        </p>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('about')
    <!-- Header -->
    <section id="about" class="large">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-7 offset-md-4 text-center block">
                    <h2 class="section-heading text-uppercase">Mijn Achtergrond</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                    <hr>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dignissimos dolores eligendi quibusdam quos. Autem commodi cum cupiditate esse eveniet fuga illum nemo possimus quo ratione sit, vero. Expedita, nemo.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequuntur debitis laudantium omnis quae quaerat quam sapiente? Adipisci consequatur delectus earum fuga inventore maxime necessitatibus nesciunt nihil, nulla rem sed.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid architecto asperiores beatae corporis culpa dolor, dolores est ipsum iure laudantium molestias praesentium quae quia repudiandae sequi tempore ullam velit voluptas?
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid architecto asperiores beatae corporis culpa dolor, dolores est ipsum iure laudantium molestias praesentium quae quia repudiandae sequi tempore ullam velit voluptas?
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dignissimos dolores eligendi quibusdam quos. Autem commodi cum cupiditate esse eveniet fuga illum nemo possimus quo ratione sit, vero. Expedita, nemo.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequuntur debitis laudantium omnis quae quaerat quam sapiente? Adipisci consequatur delectus earum fuga inventore maxime necessitatibus nesciunt nihil, nulla rem sed.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dignissimos dolores eligendi quibusdam quos. Autem commodi cum cupiditate esse eveniet fuga illum nemo possimus quo ratione sit, vero. Expedita, nemo.
                    </p>
                    <hr>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('photos')
    <!-- Portfolio Grid -->
    <section class="large" id="portfolio">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Foto`s van de afgelopen events</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal" href="#Modal1">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img
                            class="img-fluid b-lazy"
                            src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                            data-src="/images/portfolio/temp1.jpg"
                            alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                        <p class="text-muted">04 Juni</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal" href="#Modal2">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid b-lazy"
                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                             data-src="/images/portfolio/temp3.jpg"
                             alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                        <p class="text-muted">04 Juni</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal" href="#Modal3">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid b-lazy"
                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                             data-src="/images/portfolio/temp4.jpg"
                             alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                        <p class="text-muted">04 Juni</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal" href="#Modal4">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid b-lazy"
                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                             data-src="/images/portfolio/temp5.jpg"
                             alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 class="media-heading">Lorem ipsum dolor asit amet</h4>
                        <p class="text-muted">04 Juni</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('contact')
    <!-- Contact -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Laat van u horen!</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form id="contactForm" name="sentMessage" method="POST" action="{{ route('lead.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" id="name" name="name" type="text" placeholder="Uw naam *" required data-validation-required-message="Helaas mag dit veld niet leeg zijn.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="email" name="email" type="email" placeholder="Uw Email adres *" required data-validation-required-message="Helaas mag dit veld niet leeg zijn.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="phone" name="phone" type="tel" placeholder="Uw telefoonnummer of gsm *" required data-validation-required-message="Helaas mag dit veld niet leeg zijn.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" placeholder="Uw bericht *" required data-validation-required-message="Helaas mag dit veld niet leeg zijn."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Verstuur bericht</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('models')

    <!-- Login Model -->
    <div class="modal fade" id="loginModel" tabindex="-1" role="dialog" aria-labelledby="loginModel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Inloggen</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @include('shared.forms.login')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Register Model -->
    <div class="modal fade" id="registerModel" tabindex="-1" role="dialog" aria-labelledby="registerModel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Registreren</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @include('shared.forms.register')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col mx-auto">
                            <div class="modal-header">
                                <h5 class="modal-title text-uppercase" id="exampleModalLabel">Titel Event</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0">
                                        <img class="b-lazy img-fluid d-block mx-auto rounded"
                                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                             data-src="/images/portfolio/temp1.jpg"
                                             alt="">
                                    </div>
                                    <div class="col-sm-12 col-lg-6 d-flex flex-column justify-content-between">
                                        <article>
                                            <ul class="list-inline">
                                                <li>Date: January 2017</li>
                                                <li>Client: Explore</li>
                                                <li>Category: Graphic Design</li>
                                            </ul>
                                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        </article>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fa fa-times"></i>
                                                Sluiten</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 2 -->
    <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col mx-auto">
                            <div class="modal-header">
                                <h5 class="modal-title text-uppercase" id="exampleModalLabel">Event Titel</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0">
                                        <img class="b-lazy img-fluid d-block mx-auto rounded"
                                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                             data-src="/images/portfolio/temp3.jpg"
                                             alt="">
                                    </div>
                                    <div class="col-sm-12 col-lg-6 d-flex flex-column justify-content-between">
                                        <article>
                                            <ul class="list-inline">
                                                <li>Date: January 2017</li>
                                                <li>Client: Explore</li>
                                                <li>Category: Graphic Design</li>
                                            </ul>
                                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        </article>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fa fa-times"></i>
                                                Sluiten</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 3 -->
    <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col mx-auto">
                            <div class="modal-header">
                                <h5 class="modal-title text-uppercase" id="exampleModalLabel">Event Titel</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0">
                                        <img class="b-lazy img-fluid d-block mx-auto rounded"
                                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                             data-src="/images/portfolio/temp4.jpg"
                                             alt="">
                                    </div>
                                    <div class="col-sm-12 col-lg-6 d-flex flex-column justify-content-between">
                                        <article>
                                            <ul class="list-inline">
                                                <li>Date: January 2017</li>
                                                <li>Client: Explore</li>
                                                <li>Category: Graphic Design</li>
                                            </ul>
                                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        </article>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fa fa-times"></i>
                                                Sluiten</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 4 -->
    <div class="modal fade" id="Modal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col mx-auto">
                            <div class="modal-header">
                                <h5 class="modal-title text-uppercase" id="exampleModalLabel">Event Titel</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0">
                                        <img class="b-lazy img-fluid d-block mx-auto rounded"
                                             src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                             data-src="/images/portfolio/temp5.jpg"
                                             alt="">
                                    </div>
                                    <div class="col-sm-12 col-lg-6 d-flex flex-column justify-content-between">
                                        <article>
                                            <ul class="list-inline">
                                                <li>Date: January 2017</li>
                                                <li>Client: Explore</li>
                                                <li>Category: Graphic Design</li>
                                            </ul>
                                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        </article>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fa fa-times"></i>
                                                Sluiten</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
