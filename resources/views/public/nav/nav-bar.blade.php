<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top shadow @if(Request::is('/'))  @else navbar-shrink @endif" id="mainNav">
    <div class="container-fluid no-padding">
        <a class="navbar-brand js-scroll-trigger" href=@if(Request::is('/'))"#page-top"@else "/" @endif>{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto centered">
                @if(Request::is('/'))
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#reservation">Boekingen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#calendar">Agenda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#extra">Extra</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">Achtergrond</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#portfolio">Foto`s</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('home')}}">Homepagina</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('user.appointments')}}">Boekingen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('user.inbox')}}">Berichten</a>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav text-uppercase ml-auto">
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->full_name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('user.profile') }}">
                                    {{ __('Mijn account') }}
                                </a>
                                @if(Auth::user()->hasRole('admin'))
                                    <a class="dropdown-item" href="{{ route('admin.dashboard') }}">
                                        {{ __('Admin Panel') }}
                                    </a>
                                @endif
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Uitloggen') }}
                                </a>
                                <form action="{{ route('logout') }}" id="logout-form"  method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#loginModel" href="#loginModel">
                                Inloggen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#registerModel" href="#registerModel">
                               Registreren
                            </a>
                        </li>
                    @endauth
                @endif

            </ul>
        </div>
    </div>
</nav>
