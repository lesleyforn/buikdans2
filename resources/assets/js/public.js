import '../../../node_modules/bootstrap/dist/js/bootstrap';
import '../../../node_modules/jquery-smooth-scroll/src/jquery.smooth-scroll';
import '../../../node_modules/blazy/blazy';

// Initialize
let bLazy = new Blazy({
    selector: 'img' // all images
});



(function($) {
    "use strict"; // Start of use strict

    let modelPortFolio = $('.portfolio-modal');
    let collapse = $('.collapse');
    let navbar = $(".navbar");

    $('.modal').on('shown.bs.modal', function (e) {
        let bLazy2 = new Blazy({
            selector: 'img' // all images
        });
    });

    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').smoothScroll();

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Hide navbar when modals trigger
    modelPortFolio.on('show.bs.modal', function(e) {
        navbar.addClass("d-none");
    });

    modelPortFolio.on('hidden.bs.modal', function(e) {
        navbar.removeClass("d-none");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 56
    });

    // Always 1 tab open off accordion bootstrap
    collapse.on('hidden.bs.collapse', function () {
        let defaultDiv = $($(this).data("parent")).data("default");
        collapse.eq(defaultDiv-1).collapse('show');
    });

    $(document).ready(function() {

        if(window.location.href.indexOf('#login') != -1) {
            $('#loginModel').modal('show');
        }

    });

})(jQuery); // End of use strict
