(function() {
    let delay = false;
    let mainNav = $("#mainNav");

    function setDelay(){
        delay = true;

        setTimeout(function () {
            delay = false
        }, 1000)
    }

    function mouseScroller(event){
        if($(this).width() >= 1200) {

            event.preventDefault();
            if (delay) return;
            setDelay();

            let wd = event.originalEvent.wheelDelta || -event.originalEvent.detail;

            let a = document.getElementsByTagName('section');
            if (wd < 0) {
                for (var i = 0; i < a.length; i++) {
                    let t = a[i].getClientRects()[0].top;
                    if (t >= 40) break;
                }
            }
            else {
                for (var i = a.length - 1; i >= 0; i--) {
                    let t = a[i].getClientRects()[0].top;
                    if (t < -20) break;
                }
            }

            if (i >= 0 && i < a.length) {
                $('html,body').animate({
                    scrollTop: a[i].offsetTop
                });
            }
        }
    }

    $(document).on('mousewheel DOMMouseScroll', function (event) {
        mouseScroller(event);
    });

    $('.js-scroll-trigger').click(function(){
        setDelay();
    });

    $(document).bind("touchstart", function(e) {
        if($(this).width() >= 1200) {
            e.preventDefault();
        }
    })

    // Collapse Navbar
    let navbarCollapse = function() {

        if (mainNav.offset().top <= 100) {
            mainNav.removeClass("navbar-shrink");
        } else {
            mainNav.addClass("navbar-shrink");
        }
    };

    // Collapse now if page is not at top
    navbarCollapse();

    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

})(jQuery);



/*

import '../../../node_modules/fullpage.js/dist/jquery.fullpage.js';

(function() {
    $(document).ready(function(){
        $('#fullpage').fullpage();
    });
})(jQuery);
*/
