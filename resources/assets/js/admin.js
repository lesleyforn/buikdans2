import '../../../node_modules/bootstrap/dist/js/bootstrap';
import'../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';

(function($) {
    "use strict"; // Start of use strict

    //SideNav
    $(document).ready(function () {
        var sidebarButton = $('.sidebarCollapse');
        // open or close Sidebar
        sidebarButton.on('click touch', function () {
            $('.sidebar').toggleClass('active');
            $('.sidebarCollapse').toggleClass('active');
        });

        sidebarButton.on('touchdown', function () {
            $('.sidebarCollapse').toggleClass('hover');
        });


        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });

        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });
    });


    $(window).on('load resize', function() {
        if($(this).width() <= 600) {
            $('.sidebar').addClass('active');
            $('.sidebarCollapse').addClass('active');
        }
    });


})(jQuery); // End of use strict
