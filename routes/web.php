<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

/*
 *** PUBLIC WEBSITE (views):
 */
Route::get('/', 'PublicViewController@showHome')->name('home');
Route::post('/lead', 'Message\LeadController@store')->name('lead.store');

Auth::routes();

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);

/*
 *** USERS PANEL (views):
 */
Route::prefix('user')->group(function () {
    Route::name('user.')->group(function () {

        Route::prefix('profile')->group(function () {
            Route::get('/', 'UserViewController@profile')->name('profile');

            Route::name('profile.')->group(function () {
                Route::get('edit', 'UserViewController@editProfile')->name('edit');
                Route::get('/password/change', 'UserViewController@showChangePassword')->name('password.change');

                Route::prefix('address')->group(function () {
                    Route::name('address.')->group(function () {
                        Route::get('/create', 'UserViewController@createAddress')->name('new');
                        Route::get('{address}', 'UserViewController@showAddress')->name('detail');
                        Route::get('{address}/edit', 'UserViewController@editAddress')->name('edit');
                    });
                });
            });
        });

        Route::prefix('address')->group(function () {
            Route::name('address.')->group(function () {
                Route::get('{address}', 'UserViewController@showAddress')->name('detail');
                Route::get('{address}/edit', 'UserViewController@editAddress')->name('edit');
            });
        });

        Route::get('appointments', 'UserViewController@appointments')->name('appointments');

        Route::prefix('mailbox')->group(function () {
            Route::get('/', 'UserViewController@showInbox')->name('inbox');

            Route::name('mailbox.')->group(function () {
                Route::get('create', 'UserViewController@createMessage')->name('new');
                Route::get('history', 'UserViewController@showHistory')->name('history');
                Route::get('{message}', 'UserViewController@showMessage')->name('detail');
            });
        });
    });
});

/*
 *** ADMIN PANEL (views):
 */
Route::prefix('admin')->group(function () {

    Route::name('admin.')->group(function () {
        Route::get('/', 'AdminViewController@showDashboard')->name('dashboard');

        Route::prefix('profile')->group(function () {
            Route::get('/', 'AdminViewController@showProfile')->name('profile');

            Route::name('profile.')->group(function () {
                Route::get('edit', 'AdminViewController@editProfile')->name('edit');
                Route::get('/password/change', 'AdminViewController@showChangePassword')->name('password.change');
                Route::get('/password/changed', 'AdminViewController@showChangePassword')->name('password.changed');

                Route::prefix('address')->group(function () {
                    Route::name('address.')->group(function () {
                        Route::get('/create', 'AdminViewController@createAddress')->name('new');
                        Route::get('{address}', 'AdminViewController@showAddress')->name('detail');
                        Route::get('{address}/edit', 'AdminViewController@editAddress')->name('edit');
                    });
                });
            });
        });

        Route::prefix('addresses')->group(function () {
            Route::get('/', 'AdminViewController@showAddresses')->name('addresses');
        });

        Route::prefix('address')->group(function () {
            Route::name('address.')->group(function () {
                Route::get('{address}', 'AdminViewController@showAddress')->name('detail');
                Route::get('{address}/edit', 'AdminViewController@editAddress')->name('edit');
            });
        });



        Route::prefix('mailbox')->group(function () {
            Route::get('/', 'AdminViewController@showInbox')->name('inbox');

            Route::name('mailbox.')->group(function () {
                Route::get('create', 'AdminViewController@createMessage')->name('new');
                Route::get('history', 'AdminViewController@showHistory')->name('history');
                Route::get('{message}', 'AdminViewController@showMessage')->name('detail');
            });
        });

        Route::prefix('appointments')->group(function () {
            Route::get('/', 'AdminViewController@showAppointmentsCalendar')->name('appointments');

            Route::name('appointments.')->group(function () {
                Route::get('pending', 'AdminViewController@showAppointmentsPending')->name('pending');
                Route::get('history', 'AdminViewController@showAppointmentsHistory')->name('history');
            });
        });

        Route::prefix('appointment')->group(function () {
            Route::name('appointment.')->group(function () {
                Route::get('create', 'AdminViewController@createAppointment')->name('new');
                Route::get('{appointment}', 'AdminViewController@showAppointment')->name('detail');
                Route::get('{appointment}/edit', 'AdminViewController@editAppointment')->name('edit');
            });
        });

        Route::prefix('users')->group(function () {
            Route::get('/', 'AdminViewController@showUsers')->name('users');
        });

        Route::prefix('user')->group(function () {
            Route::name('user.')->group(function () {
                Route::get('{user}', 'AdminViewController@showProfile')->name('detail');
                Route::get('{user}/edit', 'AdminViewController@editProfile')->name('edit');
                Route::get('{user}/role/edit', 'AdminViewController@showChangeRole')->name('role.edit');
                Route::get('{user}/address/new', 'AdminViewController@createAddress')->name('address.new');
                Route::get('address/{address}', 'AdminViewController@showAddress')->name('address.detail');
                Route::get('address/{address}/edit', 'AdminViewController@editAddress')->name('address.edit');
            });
        });
    });

});



/*
 *** MODELS RESOURCE CONTROLLERS:
 */
Route::resource('address', 'Address\AddressController')->except('index', 'show' , 'create', 'edit');
Route::resource('message', 'Message\MessageController')->except('index', 'show' , 'create', 'edit');
Route::resource('users', 'User\UsersController')->except('index', 'create', 'show', 'edit', 'store');
Route::resource('appointment', 'Appointment\AppointmentController')->except('index', 'show' , 'create', 'edit');




Route::put('users/{user}/pass', 'User\UsersController@changePassword')->name('users.change.password');
Route::put('users/{user}/role', 'User\UsersController@changeRole')->name('users.change.role');
Route::get('users/{user}', 'User\UsersController@activate')->name('users.activate');
Route::get('/message/{recipient}/unread', 'Message\MessageController@updateIsUnread')->name('message.update.unread');
Route::get('/message/{recipient}/isread', 'Message\MessageController@updateIsRead')->name('message.update.read');
Route::delete('/message/{message}/{recipient}', 'Message\MessageController@destroyRecipient')->name('message.destroy.recipient');
