<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address_type', 'line_1', 'line_2', 'line_3', 'zip_code', 'city', 'misc'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }


    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }
}
