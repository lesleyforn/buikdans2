<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function appointments()
    {
        return $this->belongsToMany(Appointment::class);
    }
}
