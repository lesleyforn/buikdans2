<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'last_login',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'first_name', 'last_name', 'email', 'password', 'login_counter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'login_counter'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class);
    }

    public function messages()
    {
        return $this->belongsToMany(Message::class);
    }

    public function recipients()
    {
        return $this->HasMany(Recipient::class);
    }

    public function avatar(){
        return $this->hasOne(Avatar::class);
    }

    public function appointments()
    {
        return $this->belongsToMany(Appointment::class);
    }


    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        return false;
    }

    /**
     * Check multiple roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public static function getCurrentUser(){
        return User::find(Auth::user()->id);
    }

    public static function getHomeAddress(){
        return self::getCurrentUser()->addresses->where('address_type', '=' , 'Home')->first();
    }

    public static function getIncommingMessages(){
        return self::getCurrentUser()->recipients()->get();
    }

    public static function getMessagesHistory(){
        return self::getCurrentUser()->messages()->get();
    }
}
