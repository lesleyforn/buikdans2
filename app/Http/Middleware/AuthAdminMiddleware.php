<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if($request->user()->authorizeRoles(['admin']) && $request->user()->is_active === 1){
                return $next($request);
            }
        }

        return redirect()->route('auth.login');
    }
}
