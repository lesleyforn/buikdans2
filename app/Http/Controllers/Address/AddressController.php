<?php

namespace App\Http\Controllers\Address;

use App\Address;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function validateAll(Request $request){
        request()->validate([
            'address_type' => 'required|string|max:10',
            'line_1' => 'required|string|max:255',
            'line_2' => 'required|string|max:255',
            'line_3' => 'max:255',
            'zip_code' => 'required|string|max:10',
            'city' => 'required|string|max:255',
            'country' => 'required|string|max:1'
        ]);

        return null;
    }

    /**
     * @param Request $request
     * @return null
     */
    public function validateHome(Request $request){
        request()->validate([
            'line_1' => 'required|string|max:255',
            'line_2' => 'required|string|max:255',
            'zip_code' => 'required|string|max:10',
            'city' => 'required|string|max:255',
            'country' => 'required|string|max:1'
        ]);

        return null;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        AddressController::validateAll($request);

        if ($request->session()->has('user_id')) {
            $user = User::find($request->session()->get('user_id'));
            $request->session()->forget('user_id');
        }else{
            $user = Auth::user();
        }

        $address = new Address;
        $address->address_type = $request->address_type;
        $address->created_by_user_id = $user->id;
        $address->line_1 = $request->line_1;
        $address->line_2 = $request->line_2;
        $address->line_3 = $request->line_3;
        $address->zip_code = $request->zip_code;
        $address->city = $request->city;
        $address->misc = $request->misc;
        $address->save();

        $address->countries()->attach($request->country);

        $user->addresses()->attach($address->id);

        return back()->with('status', 'Success! Address has been added to your account!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(Address $address, Request $request)
    {

        if ($request->session()->has('address_type')) {
            AddressController::validateHome($request);
            $address->address_type = $request->session()->get('address_type');
            $request->session()->forget('address_type');
        }else{
            AddressController::validateAll($request);
            if($request->address_type != $address->address_type){
                $address->address_type = $request->address_type;
            }
        }

        $country = $address->countries->first();

        $address->line_1 = $request->line_1;
        $address->line_2 = $request->line_2;
        $address->zip_code = $request->zip_code;
        $address->city = $request->city;
        $address->line_3 = $request->line_3;
        $address->misc = $request->misc;
        $address->save();

        if($country->id !== $request->country){
            $address->countries()->detach($country->id);
            $address->countries()->attach($request->country);
        }

        return back()->with('status', 'Success! Address details have been updated!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Address $address)
    {
        $user = User::findOrFail($address->created_by_user_id);

        $address->is_active = 0;
        $address->save();
        $user->addresses()->detach($address->id);

        return  back()->with('status', 'Success! Address details have been deleted!');
    }
}
