<?php

namespace App\Http\Controllers\User;

use App\Avatar;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        request()->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'avatar' => 'max:2048',
        ]);

        if($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/avatar', $filename);

            if($user->avatar()->first() === null){
                $avatar = new Avatar();
            }else{
                $avatar = $user->avatar()->first();
            }
            $avatar->filepath = $filename;
            $user->avatar()->save($avatar);
        };

        $full_name = $request->first_name .' '.$request->last_name;
        $user->full_name = $full_name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;

        $user->save();

        return back()->with('status', 'Success! Account details have been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function activate(User $user)
    {
        $user->is_active = 1;
        $user->save();

        return back()->with('status', 'Success! User has been activated!');
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(User $user, Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("failed","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("failed","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeRole(User $user, Request $request){

        switch ($request->role) {
            case 1:
                $user->roles()->sync([1]);
                break;
            case 2:
                $user->roles()->sync([1,2]);
                break;
            case 3:
                $user->roles()->sync([1,2,3]);
                break;
            case 4:
                $user->roles()->sync([1,2,3,4]);
                break;
        }

        return redirect()->back()->with("success","Role has been updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->is_active = 0;
        $user->save();

        if(Auth::user()->id === $user->id){
            Auth::logout();
        }

        return back()->with('status', 'Success! Account details have been updated!');

    }
}
