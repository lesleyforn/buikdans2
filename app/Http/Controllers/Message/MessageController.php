<?php

namespace App\Http\Controllers\Message;

use App\Message;
use App\Http\Controllers\Controller;

use App\Recipient;
use App\User;
use DateInterval;
use DateTime;
use http\Exception;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if(isset($request->full_name)){
            request()->validate([
                'full_name' => 'required',
                'subject' => 'required|string|max:255',
                'message_body' => 'required|string|max:255'
            ]);

            $to = User::where('full_name', $request->full_name)->first();
        }else{
            request()->validate([
                'subject' => 'required|string|max:255',
                'message_body' => 'required|string|max:255'
            ]);

            $to = User::where('full_name', 'Administrator')->first();
        }

        try{

            $user = User::getCurrentUser();
            if(!isset($to)){
                return back()->with('failed', 'Failed! User is unknown!')->withInput();
            }

            /*if($to->id === $user->id){
                return back()->with('failed', 'You cant send a message to your self!')->withInput();
            }*/

            $message = new Message;
            $recipient = new Recipient;

            $dt = new DateTime();
            $time = $dt->add(DateInterval::createFromDateString('+2 hours'));

            $message->subject = $request->subject;
            $message->message_body = $request->subject;
            $message->creator_user_id = $user->id;
            $message->recipient_user_id = $to->id;
            $message->created_at = $time->format('Y-m-d H:i:s');
            $message->updated_at = $time->format('Y-m-d H:i:s');

            $recipient->message_id = $message->id;
            $recipient->user_id = $to->id;
            $recipient->is_read = 0;

            $user->messages()->save($message);
            $message->recipient()->save($recipient);

            return back()->with('status', 'Success! Message has been send!');

        }catch (exception $e){
            return back()->with('status', 'Something went wrong! Please retry again.');
        }
    }

    /**
     * Update the is_read value of the recipient.
     *
     * @param Recipient $recipient
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateIsRead(Recipient $recipient)
    {
        try{
        $recipient->is_read = 1;
        $recipient->save();
            return back()->with('status', 'Success! Message has been marked read!');
        }catch (exception $e){
            return back()->with('status', 'Something went wrong! Please retry again.');
        }
    }

    /**
     * Update the is_read value of the recipient.
     *
     * @param Recipient $recipient
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateIsUnRead(Recipient $recipient)
    {
        try{
            $recipient->is_read = 0;
            $recipient->save();
            return back()->with('status', 'Success! Message has been marked unread!');
        }catch (exception $e){
            return back()->with('status', 'Something went wrong! Please retry again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $user = User::getCurrentUser();
        $user->messages()->detach($message->id);

        return back()->with('success', 'Message has been deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Message $message
     * @param Recipient $recipient
     * @return \Illuminate\Http\Response
     */
    public function destroyRecipient(Message $message, Recipient $recipient)
    {
        $recipient->user_id = null;
        $recipient->save();

        return back()->with('success', 'Message has been deleted');
    }
}
