<?php

namespace App\Http\Controllers\Message;

use App\Lead;
use http\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Lead[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Lead::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Lead|string
     */
    public function store(Request $request)
    {
        try{
            $lead = new Lead;

            $lead->name = $request->name;
            $lead->email = $request->email;
            $lead->phone = $request->phone;
            $lead->message_body = $request->message;

            $lead->save();

            return redirect('/#contact')->with('status', 'Success! We will contact you as soon as humanly possible.');

        }catch (exception $e){
            return back()->with('status', 'Something went wrong! Please retry again.');;
        }
    }
}
