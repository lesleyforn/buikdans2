<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicViewController extends Controller
{
    public function showHome()
    {
        return view('public.welcome');
    }
}
