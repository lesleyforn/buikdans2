<?php

namespace App\Http\Controllers;

use App\Address;
use App\Appointment;
use App\Country;
use App\Message;
use App\Role;
use App\User;

use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminViewController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.writer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showDashboard()
    {
        $user = Auth::user();

        return view('admin_panel.dashboard', compact('user'));
    }

    /**
     * Show all users.
     *
     * @return \Illuminate\Http\Response
     */
    public function showUsers()
    {
        $users = User::all();

        return view('admin_panel.profile.all',compact('users'));
    }

    /**
     * Show the users profile.
     *
     * @param \Illuminate\Http\Request $request
     * @param User|null $user
     * @return \Illuminate\Http\Response
     */
    public function showProfile(User $user = null, Request $request)
    {
        if($user === null){
            $user = User::getCurrentUser();
        }

        $request->session()->put('user_id', $user->id);
        $homeAddress = User::getHomeAddress();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $appointments = $user->appointments()->where('start_time', '>=' , $now)->get();

        return view('admin_panel.profile.details',compact('user', 'homeAddress', 'appointments'));
    }

    /**
     * Show edit users profile form.
     *
     * @param User|null $user
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function editProfile(User $user = null, Request $request)
    {
        $request->session()->put('address_type', 'Home');

        if($user === null){
            $user = User::getCurrentUser();
        }

        $countries = Country::all();
        $homeAddress = User::getHomeAddress();

        return view('admin_panel.profile.form', compact('user', 'homeAddress', 'countries'));
    }

    /**
     * @param User|null $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangeRole(User $user = null, Request $request)
    {
        if($user === null){
            $user = User::getCurrentUser();
        }

        $roles = Role::all();
        return view('admin_panel.profile.role', compact('user', 'roles'));
    }

    /**
     * @param User|null $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePassword(User $user = null, Request $request)
    {
        if($user === null){
            $user = User::getCurrentUser();
        }

        return view('admin_panel.profile.change_password', compact('user'));
    }


    /**
     * Show create Address form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAddresses()
    {
        $countries = Country::all();
        $addresses = Address::all();
        $addresses = $addresses->sortByDesc('is_active')->sortBy('created_by_user_id');
        $users = User::all();

        return view('admin_panel.addresses.all', compact('addresses','countries', 'users'));
    }

    /**
     * Show create Address form.
     *
     * @param \Illuminate\Http\Request $request
     * @param User|null $user
     * @return \Illuminate\Http\Response
     */
    public function createAddress(Request $request, User $user = null)
    {
        if($user === null){
            $request->session()->put('user_id', Auth::user()->id);
        }else{
            $request->session()->put('user_id', $user->id);
        }

        $countries = Country::all();

        return view('admin_panel.addresses.form', compact('countries', 'user'));
    }

    /**
     * Show address details.
     *
     * @param \Illuminate\Http\Request $request
     * @param Address $address
     * @return \Illuminate\Http\Response
     */
    public function showAddress(Request $request, Address $address)
    {

        $countries = Country::all();
        $users = User::all();

        return view('admin_panel.addresses.detail', compact('countries', 'address', 'users'));
    }

    /**
     * Show edit Address form.
     *
     * @param Address $address
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editAddress(Request $request, Address $address)
    {
        if ($request->session()->has('user_id')) {
            $user = User::find($request->session()->get('user_id'));
        }

        $countries = Country::all();

        return view('admin_panel.addresses.form', compact('countries', 'address', 'url', 'user'));
    }

    /**
     * Show create Message form.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMessage()
    {
        $users = User::all();

        return view('admin_panel.messages.form', compact('users'));
    }

    /**
     * Show all Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function showInbox()
    {
        $users = User::all();
        $messages = User::getIncommingMessages();

        return view('admin_panel.messages.inbox', compact('messages', 'users'));
    }

    /**
     * Show Send Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHistory()
    {
        $users = User::all();
        $messages = User::getMessagesHistory();

        return view('admin_panel.messages.history', compact('messages', 'users'));
    }

    /**
     * Show all Appointments.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAppointmentsCalendar()
    {
        $user = User::getCurrentUser();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $appointments = $user->appointments()->where('start_time', '>=' , $now)->get();


        return view('admin_panel.appointments.calendar', compact('appointments'));
    }

    /**
     * Show create Appointment form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAppointmentsPending()
    {
        $user = User::getCurrentUser();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $appointments = $user->appointments()->where('start_time', '>=' , $now)->get();

        return view('admin_panel.appointments.pending', compact('appointments'));
    }

    /**
     * Show Appointment details.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAppointmentsHistory()
    {
        $user = User::getCurrentUser();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $appointments = $user->appointments()->where('start_time', '<=' , $now)->get();

        return view('admin_panel.appointments.history', compact('appointments'));
    }

    /**
     * Show edit Appointment form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editAppointment()
    {
        return null;
    }

}
