<?php

namespace App\Http\Controllers;

use App\Address;
use App\Country;
use App\User;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserViewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_panel.home');
    }

    public function profile(){

        $user = User::getCurrentUser();
        $homeAddress = User::getHomeAddress();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $appointments = $user->appointments()->where('start_time', '>=' , $now)->get();

        return view('user_panel.profile.details', compact('user', 'homeAddress', 'appointments'));
    }

    /**
     * Show edit users profile form.
     *
     * @param User|null $user
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function editProfile(User $user = null, Request $request)
    {
        $request->session()->put('address_type', 'Home');

        if($user === null){
            $user = User::getCurrentUser();
        }

        $countries = Country::all();
        $homeAddress = User::getHomeAddress();

        return view('user_panel.profile.form', compact('user', 'homeAddress', 'countries'));
    }

    /**
     * @param User|null $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePassword(User $user = null, Request $request)
    {
        if($user === null){
            $user = User::getCurrentUser();
        }

        return view('user_panel.profile.change_password', compact('user'));
    }


    /**
     * Show create Address form.
     *
     * @param \Illuminate\Http\Request $request
     * @param User|null $user
     * @return \Illuminate\Http\Response
     */
    public function createAddress(Request $request, User $user = null)
    {
        if($user === null){
            $request->session()->put('user_id', Auth::user()->id);
        }else{
            $request->session()->put('user_id', $user->id);
        }

        $countries = Country::all();

        return view('user_panel.addresses.form', compact('countries', 'user'));
    }

    /**
     * Show address details.
     *
     * @param \Illuminate\Http\Request $request
     * @param Address $address
     * @return \Illuminate\Http\Response
     */
    public function showAddress(Request $request, Address $address)
    {

        $countries = Country::all();
        $users = User::all();

        return view('user_panel.addresses.detail', compact('countries', 'address', 'users'));
    }

    /**
     * Show edit Address form.
     *
     * @param Address $address
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editAddress(Request $request, Address $address)
    {
        if ($request->session()->has('user_id')) {
            $user = User::find($request->session()->get('user_id'));
        }

        $countries = Country::all();

        return view('user_panel.addresses.form', compact('countries', 'address', 'url', 'user'));
    }

    public function appointments()
    {
        $user = User::getCurrentUser();
        $dt = new DateTime();
        $now = $dt->add(DateInterval::createFromDateString('+2 hours'));

        $appointments = $user->appointments()->where('start_time', '>=' , $now)->get();


        return view('user_panel.appointments.status', compact('appointments'));
    }

    /**
     * Show all Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function showInbox()
    {
        $users = User::all();
        $messages = User::getIncommingMessages();

        return view('user_panel.messages.inbox', compact('messages', 'users'));
    }

    /**
     * Show all Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHistory()
    {
        $users = User::all();
        $messages = User::getMessagesHistory();

        return view('user_panel.messages.history', compact('messages', 'users'));
    }

    /**
     * Show Send Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMessage()
    {
        return view('user_panel.messages.create');
    }

}
