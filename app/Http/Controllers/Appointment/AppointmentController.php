<?php

namespace App\Http\Controllers\Appointment;


use App\Appointment;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        request()->validate([
            'event_title' => 'required|string|max:255',
            'start_date' => 'required|date',
            'start_time' => 'required|date_format:H:i',
            'end_time_exp' => 'required|integer',
            'description' => 'required'
        ]);

        $startTime = $request->start_date." ".$request->start_time.":00";

        $appointment = new Appointment;

        $appointment->event_title = $request->event_title;
        $appointment->description = $request->description;
        $appointment->start_time = $startTime;
        $appointment->end_time_exp = $request->end_time_ex;
        $appointment->cancelled = 0;

        $user = User::getCurrentUser();

        $user->appointments()->save($appointment);
        $appointment->status()->attach(1);

        return redirect()->back()->with("success","Appointment has been send successfully!");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, appointment $appointment)
    {
        //
    }

}
