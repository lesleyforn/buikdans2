<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Address;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'    => 'required|string|max:50',
            'last_name'     => 'required|string|max:100',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',

            'line_1'    => 'required|string|max:255',
            'line_2'    => 'string|max:255',
            'zip_code'    => 'required|string|max:10',
            'city'    => 'required|string|max:255',
            'country'    => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $name = $data['first_name'].' '.$data['last_name'];

        $user = new User();
        $address = new Address;

        $user->full_name = $name;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->is_active = true;

        $address->address_type = 'Home';
        $address->line_1 = $data['line_1'];
        $address->line_2 = $data['line_2'];
        $address->zip_code = $data['zip_code'];
        $address->city = $data['city'];
        $address->is_active = 1;

        $user->save();
        $address->save();
        $address->countries()->attach(1);
        $user->roles()->attach(Role::where('name', 'user')->first());
        $user->addresses()->attach($address->id);

        return $user;
    }
}
