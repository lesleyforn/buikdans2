<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'is_active' => 1];
    }


    /**
     * Get the post register / login redirect path.
     *
     * @param Request $request
     * @return string
     */
    public function authenticated(Request $request)
    {
        if($request->user()->is_active === 0){
            return redirect('/logout')->withInput();
        }

        $user = $request->user();
        $user->login_counter++;
        $dt = new DateTime();
        $time = $dt->add(DateInterval::createFromDateString('+2 hours'));
        $user->last_login = $time->format('Y-m-d H:i:s');
        $user->update();

        // Logic that determines where to send the user
        if($user->hasAnyRole(['writer', 'editor', 'admin'])){
            return redirect()->route('admin.profile');
        }

        if($user->hasRole('user')) {
            return redirect('/user/profile');
        }

        return redirect('/');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return redirect()->to('/#login');
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->to('/#login')
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }
}
