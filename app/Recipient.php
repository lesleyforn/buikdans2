<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    protected $table = 'message_recipient';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'is_read'
    ];

    public function message()
    {
        return $this->belongsTo('App\Message');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
