<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'message_body'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function recipient()
    {
        return $this->HasMany(Recipient::class);
    }
}
