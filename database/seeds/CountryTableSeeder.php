<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new Country;
        $country->name = 'België';
        $country->short_code = 'BE';
        $country->save();
        unset($country);

        $country = new Country;
        $country->name = 'Duitsland';
        $country->short_code = 'DE';
        $country->save();
        unset($country);

        $country = new Country;
        $country->name = 'Nederland';
        $country->short_code = 'NL';
        $country->save();
        unset($country);

    }
}
