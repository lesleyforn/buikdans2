<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new Status();
        $status->name = 'Pending';
        $status->description = 'In afwachting voor goedkeuring';
        $status->save();

        $status = new Status();
        $status->name = 'Accepted';
        $status->description = 'Uw boeking is geaccepteerd';
        $status->save();

        $status = new Status();
        $status->name = 'Cancelled';
        $status->description = 'Uw boeking is geannuleerd';
        $status->save();
    }
}
