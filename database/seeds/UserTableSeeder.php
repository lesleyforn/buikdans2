<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Address;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->full_name = 'User Name';
        $user->first_name = 'User';
        $user->last_name = 'Name';
        $user->email = 'user@site.be';
        $user->password = bcrypt('password');
        $user->save();

        $user_address = new Address();
        $user_address->created_by_user_id = '1';
        $user_address->address_type = 'Home';
        $user_address->line_1 = 'Dummy Street';
        $user_address->line_2 = '4';
        $user_address->line_3 = '';
        $user_address->zip_code = '3590';
        $user_address->city = 'Diepenbeek';
        $user_address->misc = '';
        $user_address->is_active = '1';
        $user_address->save();
        $user_address->countries()->attach(1);

        $user->roles()->attach(1);
        $user->addresses()->attach($user_address);


        $editor = new User();
        $editor->full_name = 'Yasmien Pieters';
        $editor->first_name = 'Yasmien';
        $editor->last_name = 'Pieters';
        $editor->email = 'yasmien@site.be';
        $editor->password = bcrypt('password');
        $editor->save();

        $editor_address = new Address();
        $editor_address->created_by_user_id = '2';
        $editor_address->address_type = 'Home';
        $editor_address->line_1 = 'Meidoornlaan';
        $editor_address->line_2 = '4';
        $editor_address->line_3 = '';
        $editor_address->zip_code = '3590';
        $editor_address->city = 'Diepenbeek';
        $editor_address->misc = '';
        $editor_address->is_active = '1';
        $editor_address->save();
        $editor_address->countries()->attach(1);

        $editor->roles()->sync([1, 2, 3]);
        $editor->addresses()->attach($editor_address);


        $administrator = new User();
        $administrator->full_name = 'Administrator';
        $administrator->first_name = 'Lesley';
        $administrator->last_name = 'Forn';
        $administrator->email = 'admin@site.be';
        $administrator->password = bcrypt('password');
        $administrator->save();

        $administrator_address = new Address();
        $administrator_address->created_by_user_id = '3';
        $administrator_address->address_type = 'Home';
        $administrator_address->line_1 = 'Meidoornlaan';
        $administrator_address->line_2 = '4';
        $administrator_address->line_3 = '';
        $administrator_address->zip_code = '3590';
        $administrator_address->city = 'Diepenbeek';
        $administrator_address->misc = '';
        $administrator_address->is_active = '1';
        $administrator_address->save();
        $administrator_address->countries()->attach(1);

        $administrator->roles()->sync([1, 2, 3, 4]);
        $administrator->addresses()->attach($administrator_address);
    }

}
