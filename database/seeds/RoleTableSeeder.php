<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'user';
        $role_user->description = 'A Customer User';
        $role_user->save();

        $role_writer = new Role();
        $role_writer->name = 'writer';
        $role_writer->description = 'A User with the permissions to accept or decline appointments, send users wide mail';
        $role_writer->save();

        $role_editor = new Role();
        $role_editor->name = 'editor';
        $role_editor->description = 'A Administrator User with almost all the permissions, without all the technical stuff';
        $role_editor->save();

        $role_administrator = new Role();
        $role_administrator->name = 'admin';
        $role_administrator->description = 'A Administrator User, All options available. Use with care!';
        $role_administrator->save();
  }
}
