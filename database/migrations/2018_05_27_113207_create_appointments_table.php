<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_user_id')->unsigned()->nullable();
            $table->string('event_title');
            $table->text('description');
            $table->timestamp('start_time');
            $table->timestamp('end_time_exp')->nullable();
            $table->integer('end_time')->nullable();
            $table->boolean('cancelled')->nullable();
            $table->text('cancellation_reason')->nullable();
            $table->boolean('attended')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
